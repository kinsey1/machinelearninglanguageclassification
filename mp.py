import itertools
import numpy as np
import matplotlib.pyplot as plt
import json
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix




def read_file(filename):
    '''
    Reads in a JSON twitter file and outputs a list of dictionaries
    which are similar to the JSON format
    e.g. {'lang' : 'fr', 'text' : 'hello world'}

    '''
    output = []
    with open(filename, "r") as data_file:
        for i in data_file:
            raw_inst = json.loads(i)
            inst = {}
            inst['lang'] = raw_inst['lang']
            inst['text'] = raw_inst['text']
            output.append(inst)
    return output


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')


    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def plot_numlangs():
    
    raw = read_file('train.json')
    totals = {}
    probs = {}

    for inst in raw:
        lang = inst['lang'] 
        
        if(lang in totals):
            totals[lang]+=1
        else:
            totals[lang]=1

    plt.bar(range(len(totals)), totals.values() )
    plt.xticks(range(len(totals)), totals.keys())
    plt.xlabel('Languages')
    plt.ylabel('Number of instances')
    plt.title("Instances Per Language")
    plt.show()
