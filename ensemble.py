#from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
import numpy as np
from sklearn.linear_model import SGDClassifier
import pprint
import classifier as cl
import os
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
import psutil
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_score

'''
categories = ['alt.atheism', 'soc.religion.christian',
              'comp.graphics', 'sci.med']

twenty_train = fetch_20newsgroups(subset='train',
    categories=categories, shuffle=True, random_state=42)


print(len(twenty_train.data))
print(len(twenty_train.target))
'''


def print_targets():
    for t in twenty_train.target[:100]:
        print(twenty_train.target_names[t])




def format_data(raw_data, div = 0.7):
    ''' 
    returns a two tuple of lists; data and its targets
    of formatted data
    which can be used in tfidf and then other scikitlearn
    classifiers.
    '''
    data = []
    target = []

    for inst in raw_data:
        target.append(inst['lang'])
        data.append(inst['text'])

    split_int = int(div*len(data))

    train_x = data[0:split_int]
    train_y = target[0:split_int]
    test_x = data[split_int:]
    test_y = target[split_int:]


    return (train_x, train_y, test_x, test_y)


def score(predicted, actual):
    count = 0
    for i in range(0,len(actual)):
        if (actual[i]==predicted[i]):
            
            count+=1
        #print(actual)
        #print(predicted)

    return float(count)/float(len(predicted))



def run_classifier(clf, x_train_tf, y_train, x_test_tfidf):
    clf.fit(x_train_tf, y_train)
    predicted = clf.predict(x_test_tfidf)
    
    return predicted







def main():
    raw_data = cl.read_file("dev2.json")
    data = format_data(raw_data)



    x_train_raw = data[0]
    
    x_train = []
    for i in x_train_raw:
        x_train.append(cl.clean_string(i))
      

    y_train = data[1]

    x_test = data[2]
    y_test = data[3]

    print(len(x_train))
    print(len(y_train))

    print(len(x_test))
    print(len(y_test))

    count_vect = CountVectorizer()

    x_train_counts = count_vect.fit_transform(x_train)
    
    tfidf_transformer = TfidfTransformer().fit(x_train_counts)

    x_train_tf = tfidf_transformer.transform(x_train_counts)
    

    #bayes classifier
    #bayes_clf = MultinomialNB().fit(x_train_tf, y_train)
    svm_clf = SGDClassifier(loss='hinge', penalty='l2',
                                           alpha=1e-3, n_iter=5, random_state=42)
    dt_clf = DecisionTreeClassifier()
    randomfor_clf = RandomForestClassifier()
    

    svm_clf.fit(x_train_tf, y_train)
    dt_clf.fit(x_train_tf, y_train)

    #convert testing instances to usable data
    x_test_counts = count_vect.transform(x_test)
    x_test_tfidf = tfidf_transformer.transform(x_test_counts)

    #predicted_bayes = bayes_clf.predict(x_test_tfidf)

    predicted_svm = svm_clf.predict(x_test_tfidf)
    predicted_dt = dt_clf.predict(x_test_tfidf)

    print("random for")
    print(score(run_classifier(randomfor_clf, x_train_tf, y_train, x_test_tfidf), y_test))


    print("svm")
    print(score(predicted_svm, y_test))


    print("decision tree")
    print(score(predicted_dt, y_test))
    #print(score(predicted_bayes, y_test))


        






#main()


def voting_classifier():
    raw_data = cl.read_file("dev.json")
    data = format_data(raw_data)
    x_train_raw = data[0]
    
    x_train = []
    for i in x_train_raw:
        x_train.append(cl.clean_string(i))

    y_train = data[1]

    x_test = data[2]
    y_test = data[3]

    #convert training data to vector and tfidf
    count_vect = CountVectorizer()
    x_train_counts = count_vect.fit_transform(x_train)
    tfidf_transformer = TfidfTransformer().fit(x_train_counts)
    x_train_tf = tfidf_transformer.transform(x_train_counts)
    

    #convert testing instances to usable data
    x_test_counts = count_vect.transform(x_test)
    x_test_tfidf = tfidf_transformer.transform(x_test_counts)


    svm_clf = SGDClassifier(loss='hinge', penalty='l2',
                                    alpha=1e-3, n_iter=5, random_state=42)
    dt_clf = DecisionTreeClassifier()
    nb_clf = GaussianNB()
    

    voting1 = VotingClassifier(estimators=[
        ('svm', svm_clf), ('dt', dt_clf), ('gnb', nb_clf)], voting = 'hard')

    #voting1.fit(x_train_tf, y_train)

    #predicted = voting1.predict(x_test_tfidf)

    x = cross_val_score(voting1, x_train_tf.toarray(), y_train, scoring = 'accuracy')

    print(x)


voting_classifier()



  
'''
f = open('train.json', 'r')
out = open("train2_5k.json", 'w')
count = 0
for i in f.readlines():
    out.write(i)
    count+=1
    if(count>2500):
        break

out.close()
'''  
