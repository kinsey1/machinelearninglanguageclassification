import classifier as cl
import pprint
import json
import re
from nltk import ngrams
from sklearn.metrics import precision_score


from sklearn.metrics import precision_score
y_true = ['a', 'b', 'c', 'd', 'a', 'b']
y_pred = ['a', 'c', 'b', 'd', 'b', 'a']
print(precision_score(y_true, y_pred, labels=['a'], average=None)) 






NGRAM_SIZE = 2

def read_file(filename):
    '''
    Reads in a JSON twitter file and outputs a list of dictionaries
    which are similar to the JSON format
    e.g. {'lang' : 'fr', 'text' : 'hello world'}

    '''
    output = []
    with open(filename, "r") as data_file:
        for i in data_file:
            raw_inst = json.loads(i)
            inst = {}
            inst['lang'] = raw_inst['lang']
            inst['text'] = raw_inst['text']
            output.append(inst)
    return output

f = read_file("dev.json")
#unk = open("unk.txt")
for i in f:
    if i['lang'] == u'unk':
        exit(0)

def clean_text(raw_text, type = 'w'):
    '''
    returns a list of words which are cleaned of capitilisation,
    ";,.?'" and other erroneous characters.
    These characters are not indicative of the language.
    type = characters
    c - characters
    n - ngrams
    w - words
    s - string
    '''

    output = []
    #remove RT and @links
    stripped = []
    for i in raw_text.split():
        if not i.startswith("@") and i!="RT" and not i.startswith("http"):
            stripped.append(i)

    raw_text = " ".join(stripped)

    if(type=='w'):
        for word in raw_text.split(" "):


            clean_str = ''.join([i for i in word if i.isalpha()])
            clean_str = clean_str.lower()
            output.append(clean_str)
        
        output = filter(None, output)
    
    #splitting by characters
    elif(type=='c'):
        #raw_text = TODO clean the string in a separate func 
        chars = list(raw_text)
        output = [x.lower() for x in chars if x.isalpha()]
    
    #splitting by ngrams
    elif(type=='n'):
        
        new_str = ''
        for i in raw_text:
            if i.isalpha() or i==' ':
                new_str+=i.lower()

        output = [x[0] + x[1] for x in ngrams(new_str, NGRAM_SIZE)]
    
    elif(type=='s'):
        clean_str = ''.join([i for i in raw_text if i.isalpha() or i==' '])
        clean_str = clean_str.lower()
        output.append(clean_str)
    return output




'''

a = "hello RT @kinsey c'est en francaise bla bla bla !!!@@@ go here https://helloworld.com"


def remove_rt(str):
    out = []
    for i in str.split():
        if not i.startswith("@") and i!="RT" and not i.startswith("http"):
            out.append(i)

    return " ".join(out)

print(remove_rt(a))

'''

def randsplit():
    import random
    with open('train.json','r') as source:
        data = [ (random.random(), line) for line in source ]
    data.sort()
    with open('train_rand.json','w') as target:
        for _, line in data:
            target.write( line )




def clean_text(raw_text, type = 'w'):
    '''
    returns a list of words which are cleaned of capitilisation,
    ";,.?'" and other erroneous characters.
    These characters are not indicative of the language.
    type = characters
    c - characters
    n - ngrams
    w - words
    '''

    output = []
    #remove RT and @links
    stripped = []
    for i in raw_text.split():
        if not i.startswith("@") and i!="RT" and not i.startswith("http"):
            stripped.append(i)

    raw_text = " ".join(stripped)

    if(type=='w'):
        for word in raw_text.split(" "):


            clean_str = ''.join([i for i in word if i.isalpha()])
            clean_str = clean_str.lower()
            output.append(clean_str)
        
        output = filter(None, output)
    
    #splitting by characters
    elif(type=='c'):
        #raw_text = TODO clean the string in a separate func 
        chars = list(raw_text)
        output = [x.lower() for x in chars if x.isalpha()]
    
    #splitting by ngrams
    elif(type=='n'):
        
        new_str = ''
        for i in raw_text:
            if i.isalpha() or i==' ':
                new_str+=i.lower()

        output = [x[0] + x[1] for x in ngrams(new_str, NGRAM_SIZE)]

    #splitting by string
    

    return output


def product(in_list):
    total = in_list[0]
    for i in in_list[1:]:
        total*=i
    return float(total)








