import json
from pprint import pprint
import sys
import re
from nltk import ngrams
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
import numpy as np
import matplotlib.pyplot as plt
import math
import random
import itertools
import json
from sklearn.metrics import recall_score, precision_score, f1_score, confusion_matrix, accuracy_score
from sklearn.svm import LinearSVC



'''
Machine Learning Project 2 - Kinsey Reeves - 695705

This python file contains implementatins of 3 seperate classifiers
to classify twitter posts. 

Naive Bayes Voter:
The function run_bayes_voter() is used to run this classifier. 
This is a Naive bayes classifier that votes on the predicted label
using character, bi, and trigrams. 

Naive Bayes stacker
Converts the dataset to language scores where each instance has a list
of 60 attributes 

'''


#what to classify words that dont appear
MIN_E = 1.0/10**150

#percentage of training data to use
DATA_PERCENT = 0.2

#ngram NGRAM_SIZE
NGRAM_SIZE = 2

#validation split
SPLIT = 0.8

#if this percent of the thing has been cleaned its classified as a 'weird' case in the stacker
CLEAN_THRESH = 0.7

#characters to remove from a string. As I see it these don't often indicate language
#some however like ' are used commonly inside words e.g english, french, spanish etc
BAD_CHARS = "<>,.@#$^%&*()!~`{};:?/*"

def read_file(filename, test=False):
    '''
    Reads in a JSON twitter file and outputs a list of dictionaries
    which are similar to the JSON format
    e.g. {'lang' : 'fr', 'text' : 'hello world'}

    '''
    output = []
    with open(filename, "r") as data_file:
        for i in data_file:
            raw_inst = json.loads(i)
            inst = {}
            if('lang' in raw_inst):
                inst['lang'] = raw_inst['lang']
            if(test):
                inst['id'] = raw_inst['id']
            
            if('location' in raw_inst and raw_inst['location']!="not-given"):
                inst['text'] = raw_inst['text'] + " " + raw_inst['location']
            else:
                inst['text'] = raw_inst['text']
            
            output.append(inst)
    return output



def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def split_data(data, split=SPLIT):
    train = []
    test = []
    split = int(split*100)
    for inst in data:
        randint = random.randint(0,100)
        if(randint<split):
            train.append(inst)
        else:
            test.append(inst)

    return (train, test)


def get_stats(raw_data, split_type = 'w', ngram_len = 2):
    '''
    cleans the words in the text and outputs a dictionary containing 
    the statistics of the raw data given the languages. 
    e.g. get_stats(data, type = 'w') returns a dictionary of dictionaries
    of the word frequencies for each language
    '''
    element_freq = {}
    count = 0
    
    for inst in raw_data:
        #check the language in the dictionary
        if(inst['lang'] in element_freq):

            parsed_text = clean_text(inst['text'], split_type, ngram_len)
            dic = element_freq[inst['lang']]
            
            #add the words to the dictionary
            for item in parsed_text:
                if item in dic:
                    dic[item]+=1
                else:
                    dic[item] = 1
        else:
            #if the language doesn't exist
            new_dic = {}
            element_freq[inst['lang']] = new_dic
            parsed_text = clean_text(inst['text'], split_type, ngram_len)
            for word in parsed_text:
                    new_dic[word] = 1

        count+=1
       
    return element_freq




#todo add in option on exactly what to remove
def clean_text(raw_text, type = 'w', ngram_len = 2):
    '''
    returns a list of words which are cleaned of capitilisation,
    ";,.?'" and other erroneous characters.
    These characters are not indicative of the language.
    type = characters
    c - characters
    n - ngrams
    w - words
    '''

    output = []
    #remove RT and @links
    stripped = []
    
    for i in raw_text.split():
        if not i.startswith("@") and i!="RT" and not i.startswith("http"):
            stripped.append(i)
    
    raw_text = " ".join(stripped)

    if(type=='w'):
        for word in raw_text.split(" "):

            clean_str = ''.join([i for i in word if is_allowed_char(i)])
            clean_str = clean_str.lower()
            output.append(clean_str)
        
        output = filter(None, output)
    
    #splitting by characters
    elif(type=='c'):
        #raw_text = TODO clean the string in a separate func 
        chars = list(raw_text)
        output = [i.lower() for i in chars if is_allowed_char(i)]
    
    #splitting by ngrams
    elif(type=='n'):
        
        new_str = ''
        for i in raw_text:
            if i.isalpha() or i==' ':
                new_str+=i.lower()
        
        for x in ngrams(new_str, ngram_len):
            output.append(''.join(list(x)))
    
    return output

def is_allowed_char(char, badchars=BAD_CHARS):
    if char in badchars:
        return False
    return True


def calc_probs(test_inst, item_stats, lang_stats, divider = 'w',ngram_len = 2):
    '''
    Inputs a raw test instance
    calculates the probability of all words in a test inst and 
    averages the result. Returns a list of 3 tuples of (lang, [probs], sum(probs))
    -note that these probabilties are actually log(prob)
    params:
    item_stats - A dictinary of dictionaries; containing all languages and their own
    test_inst - A test intance of uncleaned text
    dictionaries with all the words seen previously
    lang_stats - A dictionary of all languages and their prior probabilities
    Returns a list of 3 tuples, containing (lang, probabilities, sum(log(probs)))
    from which the highest log(probability can be chosen)
    '''
    
    cleaned_items = clean_text(test_inst, divider, ngram_len)
    
    #all the conditional probabilities for the words and the language
    #a list of two tuples (lang, [P(word|lang)])
    lang_word_probs = []

    for lang in lang_stats:
        #total number of words for the given language
        total = sum(item_stats[lang].values())
        lang_dic = item_stats[lang]
        probs = []


        #for each text item ie word, ngram, char 
        for item in cleaned_items:
            if item in lang_dic:
                prob = float(lang_dic[item]) / float(total)
                probs.append(math.log(prob))
            else:
                probs.append(math.log(MIN_E))

        lang_word_probs.append((lang, probs, sum(probs)))

    return lang_word_probs



def get_highest_prob(lang_probs):
    '''
    gets the highest probability language given a list of 3 tuples
    each 3-tuple consists of (lang, [probs for each word], product(probs))
    e.g. [(u'ru', [0.00000043, 1e-07, 1e-07, 1e-07, 1e-07], 1.04-52)

    '''
    max_lang = lang_probs[0][0]
    max_prob = lang_probs[0][2]


    for prob in lang_probs:
        if(prob[2] > max_prob):
            max_prob = prob[2]
            max_lang = prob[0]

    return max_lang



def calc_lang_probs(instances):
    '''
    Calculates the prior probabilities of all the languages. 
    and returns a dictionary containing each language and its 
    probability
    Parameters:
    instances - all of the training instances (X)
    '''
    totals = {}
    probs = {}

    for inst in instances:
        lang = inst['lang'] 
        
        if(lang in totals):
            totals[lang]+=1
        else:
            totals[lang]=1
    
    total_inst = sum(totals.values())

    for lang in totals:
        probs[lang] = float(totals[lang])/float(total_inst)
    
    return probs

def clean_string(raw_text):
    '''
    cleans a string and returns it as a string. Uses
    the is_allowed_char function to determine if the 
    characters should be removed.
    '''
    output = []
    stripped = []
    for i in raw_text.split():
        if not i.startswith("@") and i!="RT" and not i.startswith("http"):
            stripped.append(i)

    raw_text = " ".join(stripped)
    clean_str = ''.join([i for i in raw_text if is_allowed_char(i) or i==' '])
    clean_str = clean_str.lower()
    return clean_str


def vote(a,b,c):
    '''
    Simple voter for my naive bayes classifier, voting on ngrams, 
    words and characters. 
    If no element wins the vote, the a argument is chosen
    '''
    if a==b or b == c :
        return b
    if a==c:
        return a
    return a



def run_bayes_voter(data_file, test_data=None, test_labels = True, 
    plot = False, print_stats = False, unk_len_thresh = 0.9, out_file = None):
    '''
    Bayesian voting classifier. votes using bigram, 
    trigram and 4gram frequencies to predict a class label.

    data_file - the input data file
    test_data - if using a seperate file for testing
    plot - plot the confusion matrix
    print_stats - print different statistics in the voting process
    unk_len_thresh - what percentage of bad chars makes us classify an inst as unknown
    '''
    data = read_file(data_file)

    if(not test_data):
        data = split_data(data)
        raw_data_train = data[0]
        raw_data_test = data[1]
    else:
        if(test_data):
            raw_data_train = data
            raw_data_test = read_file(test_data, test = True)

    lang_stats = calc_lang_probs(raw_data_train)

    #create the probability tables for each part of language ie word, char, ngram
    chars_dic = get_stats(raw_data_train, split_type = 'c')
    n2grams_dic = get_stats(raw_data_train, split_type = 'n', ngram_len = 2)
    n3grams_dic = get_stats(raw_data_train, split_type = 'n', ngram_len = 3)
    words_dic = get_stats(raw_data_train, split_type = 'w')
    
    correct_ngram = 0
    correct_char = 0
    correct_total = 0
    total = 0

    predicted = []
    pred_char = []
    pred_n2gram = []
    pred_n3gram = []
    pred_word = []
    y_test = []
    ids = []

    #loop through all instances
    for inst in range(0, len(raw_data_test)):

        test_inst = raw_data_test[inst]['text']
        if(test_labels):
            test_lang = raw_data_test[inst]['lang']
        if(out_file):
            id = raw_data_test[inst]['id']

        #lists of 3 tuples containing probs for each language given the test_inst
        char_probs = calc_probs(test_inst, chars_dic, lang_stats, divider ='c')
        ngram2_probs = calc_probs(test_inst, n2grams_dic, lang_stats, divider ='n', ngram_len=2)
        ngram3_probs = calc_probs(test_inst, n3grams_dic, lang_stats, divider ='n', ngram_len=3) 
        word_probs = calc_probs(test_inst, words_dic, lang_stats, divider = 'w')

        #predicted lang for each method (word is included as it is helpful in classifying unknowns)
        pred_lang_char = get_highest_prob(char_probs)
        pred_lang_n2gram =  get_highest_prob(ngram2_probs)
        pred_lang_n3gram = get_highest_prob(ngram3_probs)
        pred_lang_word = get_highest_prob(word_probs)
        pred_lang_vote = vote(pred_lang_char, pred_lang_n2gram, pred_lang_n3gram)

        #classifying unknown values - this should only be done when classifying test data ie estimating the unknowns rather than training
        if(test_data 
        and (len(test_inst)-len(clean_string(test_inst)) > unk_len_thresh*len(test_inst) 
        or (pred_lang_word!=pred_lang_n3gram 
        and pred_lang_char!=pred_lang_n2gram))):
            predicted.append(u'unk')
        else:
            predicted.append(pred_lang_vote)
        
        pred_n2gram.append(pred_lang_n2gram)
        pred_n3gram.append(pred_lang_n3gram)
        pred_char.append(pred_lang_char)
        pred_word.append(pred_lang_word)

        if(test_labels):
            y_test.append(test_lang)
        else:
            ids.append(id)

    if(out_file):
        out = open(out_file, "w")
        
        for i in range(0,len(predicted)):
            out.write("{},{}\n".format( ids[i], predicted[i]))
        
    
    
    if(plot):
        lab = list(set(y_test))
        cnf = confusion_matrix(y_test, predicted, labels=lab)
        plt.figure()
        plot_confusion_matrix(cnf, classes = lab, title = 'Confusion Bayes')
        plt.show()
    
    if(test_labels):
        acc = accuracy_score(y_test, predicted)
    
    if(print_stats):
        acc_char = accuracy_score(y_test,pred_char)
        acc_n2gram = accuracy_score(y_test,pred_n2gram)
        acc_n3gram = accuracy_score(y_test, pred_n3gram)
        acc_word = accuracy_score(y_test, pred_word)
        lab = list(set(y_test))
        print("char accuracy:")
        print(acc_char)
        print("acc_n2gram:")
        print(acc_n2gram)
        print("acc_n3gram:")
        print(acc_n3gram)
        print("acc word")
        print(acc_word)
        print("total bayes accuracy: ")
        print(acc)

    return (predicted,y_test)


def run_tfidf_classifier(data_file, test_data=None, test_labels = True, holdout=True, plot = False):
    '''
    runs a tfidf classifier using a SVM
    which uses stochastic gradient descent.
    Returns the predicted labels and the actual labels of the test data
    Parameters: 

    data_file - the training data file, if this is the only file given,
        the classifier will use a holdout strategy on it.
    test_data - testing data file
    test_labels - bool if the test file contains the labels to be predicted
    holdout - use the holdout strategy on the data_file (if no test data)
    plot - to output a matplotlib confusion matrix

    '''

    data = read_file(data_file)

    if(holdout):
        data = split_data(data)
        raw_data_train = data[0]
        raw_data_test = data[1]
    else:
        if(test_data):
            raw_data_train = data
            raw_data_test = read_file(test_data)

    x_train = []
    y_train = []

    x_test = []
    y_test = []


    for inst in raw_data_train:
        x_train.append(clean_string(inst['text']))
        y_train.append(inst['lang'])

    for inst in raw_data_test:
        x_test.append(clean_string(inst['text']))
        y_test.append(inst['lang'])
        
    

    tfidf = TfidfTransformer()
    count_vect = CountVectorizer()

    x_train = tfidf.fit_transform(count_vect.fit_transform(x_train))

    print(x_train.shape)

    x_test = tfidf.transform(count_vect.transform(x_test))
    
    clf1 = SGDClassifier(loss='hinge', penalty='elasticnet', alpha = 0.0001, n_iter=1000)
    clf1.fit(x_train, y_train)


    predicted = clf1.predict(x_test.toarray())

    lab = list(set(y_train))
    if(plot):
        cnf = confusion_matrix(y_test,predicted, labels=lab)
        plt.figure()
        plot_confusion_matrix(cnf, classes = lab, title = 'cnf')
        plt.show()

    return (predicted,y_test)




def run_bayes_stacker(data_file, test_data = None, test_labels = True, holdout = True, plot = False):
    '''
    Creates meta features of the languages probabilities and trains
    SGD classifier on them. Returns the predicted labels and the actual
    labels.

    Parameters: 
    data_file - the training data file, if this is the only file given,
        the classifier will use a holdout strategy on it.
    test_data - testing data file
    test_labels - bool if the test file contains the labels to be predicted
    holdout - use the holdout strategy on the data_file (if no test data)
    plot - to output a matplotlib confusion matrix
    '''

    data = read_file(data_file)

    if(not test_data):
        data = split_data(data)
        raw_data_train = data[0]
        raw_data_test = data[1]
    else:
        if(test_data):
            raw_data_train = data
            raw_data_test = read_file(test_data)

    lang_stats = calc_lang_probs(raw_data_train)

   #create the probability tables for each part of language ie word, char, ngram
    chars_dic = get_stats(raw_data_train, split_type = 'c')

    ngrams2_dic = get_stats(raw_data_train, split_type = 'n', ngram_len = 2)
    ngrams3_dic = get_stats(raw_data_train, split_type = 'n', ngram_len = 3)
    ngrams4_dic = get_stats(raw_data_train, split_type = 'n', ngram_len = 4)

    new_x_train = []
    new_y_train = []

    x_test = []
    y_test = []

    for inst in raw_data_test:
        x_test.append(inst['text'])
        y_test.append(inst['lang'])

    #now create the new training data
    for inst in raw_data_train:
        text = inst['text']
        lang = inst['lang']
        #all the probabilities for a each language given this instance
        #char_probs = calc_probs(text, chars_dic,lang_stats, divider = 'n')
        n2_probs = calc_probs(text, ngrams2_dic,lang_stats, divider = 'n', ngram_len = 2)
        n3_probs = calc_probs(text, ngrams3_dic,lang_stats, divider = 'n', ngram_len = 3)
        n4_probs = calc_probs(text, ngrams4_dic,lang_stats, divider = 'n', ngram_len = 4)
        

        new_x_train.append([x[2] for x in n2_probs] 
            + [x[2] for x in n3_probs] 
            + [x[2] for x in n4_probs]
            )
        new_y_train.append(lang)

    #clf = RandomForestClassifier(n_estimators=100, n_jobs=-1, class_weight=lang_stats)
    #clf = SGDClassifier(loss='hinge', penalty='elasticnet', alpha = 0.0001, n_iter=1000)
    clf = LinearSVC(loss='squared_hinge', C=10)
    clf.fit(new_x_train, new_y_train)

    #create new test instances so they can be used in the stacker
    
    new_x_test = []
    for text in x_test:

        #char_probs = calc_probs(text, chars_dic,lang_stats, divider = 'n')
        n2_probs = calc_probs(text, ngrams2_dic,lang_stats, divider = 'n', ngram_len = 2)
        n3_probs = calc_probs(text, ngrams3_dic,lang_stats, divider = 'n', ngram_len = 3)
        n4_probs = calc_probs(text, ngrams4_dic,lang_stats, divider = 'n', ngram_len = 4)

        
       
        new_x_test.append([x[2] for x in n2_probs] 
            + [x[2] for x in n3_probs] 
            + [x[2] for x in n4_probs] )


    y_pred = clf.predict(new_x_test)


    lab = list(set(new_y_train))
    
    if(plot):
        cnf = confusion_matrix(y_test,y_pred, labels=lab)
        plt.figure()
        plot_confusion_matrix(cnf, classes = lab, title = 'cnf')
        plt.show()
    
    return(y_pred, y_test)




def runner():
    total_acc = 0
    total_f1_macro = 0
    total_f1_micro = 0
    total_f1_weighted = 0
    runs = 1

    for i in range(0,runs):
        bayes_run = run_bayes_stacker("dev.json", plot=True)
        #bayes_run = run_bayes_voter("dev.json",plot=True,print_stats = True, )
        #bayes_run = run_bayes_voter("dev.json", plot=True)
        

        predicted = bayes_run[0]
        y_test = bayes_run[1]


        f1_macro = f1_score(y_test, predicted, average = 'macro')
        f1_micro = f1_score(y_test, predicted, average = 'micro')
        f1_weighted = f1_score(y_test, predicted, average = 'weighted')
        acc = accuracy_score(y_test, predicted)
        prec = precision_score(y_test, predicted, labels=[u'unk'], average=None)
        rec = recall_score(y_test, predicted, labels=[u'unk'], average=None)


        total_acc+=acc
        total_f1_macro += f1_macro
        total_f1_micro += f1_micro
        total_f1_weighted += f1_weighted


    print("acc")
    print(float(total_acc)/float(runs))
    print("f1 macro")
    print(float(total_f1_macro)/float(runs))
    print("f1 micro")
    print(float(total_f1_micro)/float(runs))
    print("f1 weighted")
    print(float(total_f1_weighted)/float(runs))
    print("precision")
    print(float(prec))
    print("recall")
    print(float(rec))


runner()
